#include <iostream>
#include <cstring>

#include "udpsocket.h"

#define MAXDATA  1024

using namespace std;

void UdpServer(uint port)
{
    cout << "Hello Server!" << endl;

    int  len = 0;
    char data[MAXDATA] = "";

    UDPSocket *udpsocket = new UDPSocket();
    int s = udpsocket->CreateUDPServer(port, false); //设为阻塞

    while(1){
        memset(data, '\0', sizeof(data));
        if( (len = udpsocket->RecvFrom(data, MAXDATA)) > 0){
            if(!strcmp(data, "q") ||
               !strcmp(data, "Q"))
                break;

            printf("recv data: %s\n", data);

            len = udpsocket->EchoTo(data, len);
            printf("echo data len: %d\n", len);

            // 测试下Server下的SendTo函数，测试结果：可用
            //len = udpsocket->SendTo("128.0.111.10", 4000, data, len);
            //printf("sendto data len: %d\n", len);
        }
        else
            break;
    }

    delete udpsocket;
}

void UdpClient(const char *ip, uint port)
{
    cout << "Hello Client!" << endl;

    int  len = 0;
    char data[MAXDATA] = "";

    UDPSocket *udpsocket = new UDPSocket();
    int s = udpsocket->CreateUDPClient(ip, port, false); //设为阻塞

    while(1){
        cin >> data;
        printf("input: %s\n", data);

        if( (len = udpsocket->SendTo(data, strlen(data))) > 0){
            if(!strcmp(data, "q") ||
               !strcmp(data, "Q"))
                break;

            memset(data, '\0', sizeof(data));
            if( (len = udpsocket->RecvFrom(data, MAXDATA)) > 0){
                printf("echo: %s\n", data);
            }
        }
        else
            break;
    }

    delete udpsocket;
}

void UdpBroadCastClient(uint port)
{
    cout << "Hello Broadcast!" << endl;

    int  len = 0;
    char data[MAXDATA] = "";

    UDPSocket *udpsocket = new UDPSocket();
#if 1
    int s = udpsocket->CreateUDPClient(nullptr, port, false); //设为阻塞
    udpsocket->AddBroadCast(ETH0); //加入eth0网卡的广播地址
#else
    int s = udpsocket->CreateUDPClient(nullptr, port, false); //设为阻塞
    udpsocket->AddBroadCast(); //使用255.255.255.255广播地址
    udpsocket->BindLocalAddr(ETH0);
#endif

    while(1){
        cin >> data;
        printf("input: %s\n", data);

        if( (len = udpsocket->SendTo(data, strlen(data))) > 0){
            if(!strcmp(data, "q") ||
               !strcmp(data, "Q"))
                break;

            memset(data, '\0', sizeof(data));
            if( (len = udpsocket->RecvFrom(data, MAXDATA)) > 0){
                printf("echo: %s\n", data);
            }
        }
        else
            break;
    }

    delete udpsocket;
}

void UdpBroadCastServer(uint port)
{
    int  len = 0;
    char buf[MAXDATA] = "";

    UDPSocket *udpsocket = new UDPSocket();
    udpsocket->CreateUDPServer(port, false); //设为阻塞
#if 0
    udpsocket->AddBroadCast(ETH0); //加入eth0网卡的广播地址
#else
    udpsocket->AddBroadCast(); //使用255.255.255.255广播地址
    udpsocket->BindLocalAddr(ETH0);
#endif

    while(1){
        memset(buf, '\0', sizeof(buf));
        if( (len = udpsocket->RecvFrom(buf, MAXDATA)) > 0){
            buf[len] = '\0';
            printf("recv data: %s\n", buf);
        }
    }

    delete udpsocket;
}

void UdpMulticastServer(const char *mucip, uint port) // 接收
{
    cout << "Hello Multicast Server!" << endl;

    int  len = 0;
    char data[MAXDATA] = "";

    UDPSocket *udpsocket = new UDPSocket();
    int s = udpsocket->CreateUDPServer(port, false); //设为阻塞
    udpsocket->AddMemberShip(mucip, ETH0);

    while(1){
        memset(data, '\0', sizeof(data));
        if( (len = udpsocket->RecvFrom(data, MAXDATA)) > 0){
            if(!strcmp(data, "q") ||
               !strcmp(data, "Q"))
                break;

            printf("recv data: %s\n", data);
        }
        else
            break;
    }

    delete udpsocket;
}

void UdpMulticastClient(const char *mucip, uint port) // 发送
{
    cout << "Hello Multicast Client!" << endl;

    int  len = 0;
    char data[MAXDATA] = "";

    UDPSocket *udpsocket = new UDPSocket();
    int s = udpsocket->CreateUDPClient(mucip, port, false); //设为阻塞
    udpsocket->BindLocalAddr(ETH0);

    while(1){
        memset(data, '\0', sizeof(data));
        cin >> data;
        printf("input: %s\n", data);

        if( (len = udpsocket->SendTo(data, strlen(data))) > 0)
            printf("send len: %d\n", len);
        else
            break;

        if(!strcmp(data, "q") ||
           !strcmp(data, "Q"))
            break;
    }

    delete udpsocket;
}

int main(int argc, char **argv)
{
    //UdpServer(atoi(argv[1])); // - port
    //UdpClient(argv[1], atoi(argv[2])); // - ip port
    UdpBroadCastClient(atoi(argv[1])); // - port
    //UdpBroadCastServer(atoi(argv[1])); // - port
    //UdpMulticastServer(argv[1], atoi(argv[2])); // - ip port
    //UdpMulticastClient(argv[1], atoi(argv[2])); // - ip port

    return 0;
}
