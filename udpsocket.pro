TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

#指定生成的应用程序名
#TARGET = udp_server
#TARGET = udp_client
TARGET = udp_broadcastclient
#TARGET = udp_broadcastserver
#TARGET = udp_multicastserver
#TARGET = udp_multicastclient

SOURCES += main.cpp \
    udpsocket.cpp

HEADERS += \
    udpsocket.h \
    networkinfo.h
